/**
 * CSS3 performed loading animation
 * v0.91
 *
 * @author  Oliver Tomaske <ot@mediacomcept.de>
 */
var breather = new function() {

    // document root id
    this.onId = 'body';

    // lets breather listen on ajax events
    this.onAjax = false;

    // lets breather listen on ajax events
    this.ajaxEvents = [];

    // show only one instance
    this.shown = false;

    // the processing message on splash screen
    this.message = 'Operate. Please wait ..';
    var message = this.message;

    // breather id
    var s = 'breather';

    // breather content
    var c = '<div id="bubblingG"><div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div><div class="breather-message">' + message + '</div></div>';

    this.show = function(msg) {
        if (this.shown) {
            $('.breather-message').html(msg);
            return;
        }
        if (msg) {
          message = msg;
        }
        $('#' + this.onId).append('<div id="' + s + '"></div>');
        $('#' + s).fadeIn(200, function() {
            $('#' + s).append(c);
            $('.breather-message').html(message);
        });

        this.shown = true;
    }
    this.hide = function() {
        $('#' + s).fadeOut(200, function() {
            $('#' + s).remove();
            message = this.message;
        });

        this.shown = false;
    }

}


/**
 * Global Ajax listener
 * v0.9
 *
 * @author  Oliver Tomaske <ot@mediacomcept.de>
 */

var s_ajaxListener = new Object();
s_ajaxListener.tempOpen = XMLHttpRequest.prototype.open;
s_ajaxListener.tempSend = XMLHttpRequest.prototype.send;

s_ajaxListener.callback = function (e) {
      // use this
      var t = this;

      if (!breather.onAjax) {
        return;
      }

      breather.ajaxEvents.forEach(function(i) {
          if (null !== t.url.match(i)) {
              switch(e) {
                  case 'send':
                      breather.show();
                      break;
                  case 'load':
                      breather.hide();
                      break;
              }
          }
      });
}

XMLHttpRequest.prototype.open = function(a,b) {
        if (!a) var a='';
        if (!b) var b='';
        s_ajaxListener.tempOpen.apply(this, arguments);
        s_ajaxListener.method = a;
        s_ajaxListener.url = b;
        if (a.toLowerCase() == 'get') {
          s_ajaxListener.data = b.split('?');
          s_ajaxListener.data = s_ajaxListener.data[1];
        }

        this.addEventListener("load", requestComplete);
}

XMLHttpRequest.prototype.send = function(a,b) {
        if (!a) var a='';
        if (!b) var b='';
        s_ajaxListener.tempSend.apply(this, arguments);
        if(s_ajaxListener.method.toLowerCase() == 'post')s_ajaxListener.data = a;

        s_ajaxListener.callback('send');
}

function requestComplete() {
        s_ajaxListener.callback('load');
}
